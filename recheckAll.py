#!/usr/bin/env python

import os
import re
import getpass
import getopt
import sys
import time
import signal
import json
from datetime import datetime, timedelta
from dataclasses import dataclass
from enum import Enum
import logging
logging.basicConfig(level=logging.INFO)

from contextlib import contextmanager
from rich import box
from rich.align import Align
from rich.console import Console
from rich.live import Live
from rich.table import Table
from rich.layout import Layout
from rich.panel import Panel
from rich.pretty import Pretty
from rich.bar import Bar

import qbittorrentapi

CONTINUE_FILE = "recheck_all.cont"
completed: dict = {}
log = []

def save_state():
    #print(completed)
    with open(CONTINUE_FILE, 'w') as file:
        json.dump(completed, file)


def signal_handler(sig, frame):
    save_state()
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


def print_help(argv):
    print(f"""{argv} [options]

    Check for broken torrents where no trackers are enabled or working.
    """)
    print("""Options:
    -c | --concurrent      : Delete the torrents from the client
    -v | --verbose         : Verboseness, multiple flags will increase verboseness
    -H | --host <hostname> : qBittorrent host
    -P | --port <port>     : qBittorrent port
    -u | --user <username> : qBittorrent psername
    -p | --pass <password> : qBittorrent password
    """)
    print("""Environment variables:
    QBITTORRENT_HOST     : qBittorrent host
    QBITTORRENT_PORT     : qBittorrent port
    QBITTORRENT_USERNAME : qBittorrent username
    QBITTORRENT_PASSWORD : qBittorrent password
    """)


def load_state():
    with open(CONTINUE_FILE, 'r') as file:
        return json.load(file)


def generate_layout(table_data, status):
    layout = Layout()
    layout.split_column(
        Layout(name="upper"),
        Layout(generate_table(table_data), name="status")
    )
    layout["upper"].split_row(
        Layout(generate_status(status), name="status", ratio=3),
        Layout(generate_log(), name="log", ratio=7)
    )
    return layout

def generate_log():
    table = Table(expand=True, box=box.MINIMAL_DOUBLE_HEAD)
    table.add_column("Time", no_wrap=True, ratio=1)
    table.add_column("Message", no_wrap=True, ratio=4)
    # table.add_column("Level", no_wrap=True, ratio=1)
    for row in reversed(log):
        table.add_row(*[row['time'], row['message']])
    return Panel(table, title="Log")


def generate_status(status):
    output = "# Recheck all torrents\n"
    for k in status:
        output += f"{k}: {status[k]}\n"
    panel = Panel(output, title="Info")
    return panel


def generate_table(table_data):
    table = Table(expand=True, box=box.MINIMAL_DOUBLE_HEAD)
    table.add_column("Name", no_wrap=True, ratio=13)
    table.add_column("Status", ratio=2)
    table.add_column("Progress", ratio=4)
    table.add_column("%", ratio=1)
    for row in table_data:
        table.add_row(*[row.name, row.state, Bar(size=1.0, begin=0.0, end=row.progress), f"{row.progress*100:.0f}%"])
    return Panel(table, title="Status")


def log_message(msg):
    return {'time': datetime.now().strftime("%d/%m %H:%M"), 'message': msg}


# def main(argv):
    # signal.signal(signal.SIGINT, signal_handler)
host = os.environ.get("QBITTORRENT_HOST", "localhost")
port = os.environ.get("QBITTORRENT_PORT", "8080")
username = os.environ.get("QBITTORRENT_USERNAME")
password = os.environ.get("QBITTORRENT_PASSWORD")
concurrent = 1
verbose = 0
try:
    opts, args = getopt.getopt(sys.argv[1:], "hvc:H:P:u:p:", ["help", "verbose", "concurrent=", "host=", "port=", "user=", "pass="])
except getopt.GetoptError:
    print_help(sys.argv[0])
    sys.exit(2)
for opt, arg in opts:
    match opt:
        case "-h" | "--help":
            print_help(sys.argv[0])
            sys.exit()
        case "-c" | "--concurrent":
            concurrent = int(arg)
        case "-v" | "--verbose":
            verbose += 1
        case "-H" | "--host":
            host = arg
        case "-P" | "--port":
            port = arg
        case "-u" | "--user":
            username = arg
        case "-p" | "--pass":
            password = arg
logger = logging.getLogger(__name__)
logger.info('message')

if os.path.isfile(CONTINUE_FILE):
    logger.info("Loading continue file")
    completed = load_state()

if not username:
    username = input("Username: ")
if not password:
    password = getpass.getpass("Password: ")

conn_info = dict(
    host=host,
    port=int(port),
    username=username,
    password=password,
)
qbt_client = qbittorrentapi.Client(**conn_info)

try:
    qbt_client.auth_log_in()
except qbittorrentapi.LoginFailed as e:
    print(e)
else:
    print("Successfully logged in")

print(f"qBittorrent: {qbt_client.app.version}")
print(f"qBittorrent Web API: {qbt_client.app.web_api_version}")

checking = []
# torrents_dict = qbt_client.torrents_info()
# torrents = [t for t in torrents_dict]
run = True

# Setup table
console = Console()
console.clear()

# Cleanup torrent list
# remove = []
# for torrent in torrents:
#     #torrent.sort(key=lambda v: datetime.combine(v.created_date, v.created_time))

def get_torrents():
    remove = []
    torrents_dict = qbt_client.torrents_info()
    torrents = [t for t in torrents_dict]
    for torrent in torrents:
        state = qbittorrentapi.TorrentState(torrent.state)
        if state.is_checking or torrent.hash in completed:
            if torrent.hash not in completed:
                log.append(log_message(f"New checking found: {torrent.name} ({torrent.hash})"))
                checking.append(torrent)
            elif completed[torrent.hash]['state'] == 'checking':
                log.append(log_message(f"Resuming {torrent.name} ({torrent.hash})"))
                checking.append(torrent)
            elif completed[torrent.hash]['state'] == 'complete' and state.is_checking:
                log.append(log_message(f"Checking (completed) {torrent.name} ({torrent.hash})"))
                checking.append(torrent)
            else:
                log.append(log_message(f"Skipping (completed) {torrent.name} ({torrent.hash})"))
            remove.append(torrent)
    while remove:
        torrents.remove(remove.pop(0))
    return torrents
torrents = get_torrents()
last_update = datetime.now()

# Main loop
with Live(generate_layout(checking, {"Left": len(torrents), "Completed": len(completed), "Current": len(checking)}), console=console, screen=False, refresh_per_second=1) as live:
    while run:
        remove = []
        for torrent in checking:
            torrent.sync_local()
            state = qbittorrentapi.TorrentState(torrent.state)
            if not state.is_checking:
                remove.append(torrent)
                completed[torrent.hash] = {'state': "complete", 'hash': torrent.hash, 'time': datetime.now().timestamp()}
                log.append(log_message(f"Completed {torrent.name} ({torrent.hash})"))
        while remove:
            checking.remove(remove.pop(0))
        if len(log) > 100:
            log = log[-100:]
        live.update(generate_layout(
            checking, {"Left": len(torrents), "Completed": len(completed), "Current": len(checking)}))
        if len(checking) < concurrent and 0 < len(torrents):
            torrent = torrents.pop()
            torrent.recheck()
            completed[torrent.hash] = {'state': "checking", 'hash': torrent.hash, 'time': datetime.now().timestamp()}
            checking.append(torrent)
            log.append(log_message(f"Checking {torrent.name} ({torrent.hash})"))
        time.sleep(2)
        if 0 >= len(torrents) and 0 >= len(checking):
            run = False

save_state()
qbt_client.auth_log_out()
print("Logged out")


# if __name__ == "__main__":
#     main(sys.argv)
