# qBittorrent tools

A collection of tools for managing qBittorrent

## checkBroken.py

This checks for torrents with only broken or non-working trackers with options to delete the torrents.

Use `-d` to delete the torrents and `-v` or `-vv` to show what torrents (and their trackers) was found.

### Run

```shell
> python -m venv venv
> source venv/bin/activate
> pip install -r requirements.txt
> python checkBroken.py -H https://qbittorrent.local -P 443 -u admin
Password:
Successfully logged in
qBittorrent: v4.6.4
qBittorrent Web API: 2.8.3
Found 3 torrents with only broken or disabled trackers
Logged out
```

