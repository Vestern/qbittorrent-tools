#!/usr/bin/env python

import os
import re
import getpass
import getopt
import sys

import qbittorrentapi

def print_help(argv):
    print(f"""{argv} [options]

    Check for broken torrents where no trackers are enabled or working.
    """)
    print("""Options:
    -d | --delete          : Delete the torrents from the client
    -w | --with_data       : If deleting, also delete the data
    -v | --verbose         : Verboseness, multiple flags will increase verboseness
    -H | --host <hostname> : qBittorrent host
    -P | --port <port>     : qBittorrent port
    -u | --user <username> : qBittorrent psername
    -p | --pass <password> : qBittorrent password
    """)
    print("""Environment variables:
    QBITTORRENT_HOST     : qBittorrent host
    QBITTORRENT_PORT     : qBittorrent port
    QBITTORRENT_USERNAME : qBittorrent username
    QBITTORRENT_PASSWORD : qBittorrent password
    """)

def main(argv):
    host = os.environ.get("QBITTORRENT_HOST", "localhost")
    port = os.environ.get("QBITTORRENT_PORT", "8080")
    username = os.environ.get("QBITTORRENT_USERNAME")
    password = os.environ.get("QBITTORRENT_PASSWORD")
    delete = False
    with_data = False
    verbose = 0
    try:
        opts, args = getopt.getopt(argv[1:], "hdwvH:P:u:p:", ["help", "delete", "with-data", "verbose", "host=", "port=", "user=", "pass="])
    except getopt.GetoptError:
        print_help(argv[0])
        sys.exit(2)
    for opt, arg in opts:
        match opt:
            case "-h" | "--help":
                print_help(argv[0])
                sys.exit()
            case "-d" | "--delete":
                delete = True
            case "-w" | "--with-data":
                with_data = True
            case "-v" | "--verbose":
                verbose += 1
            case "-H" | "--host":
                host = arg
            case "-P" | "--port":
                port = arg
            case "-u" | "--user":
                username = arg
            case "-p" | "--pass":
                password = arg

    if not username:
        username = input("Username: ")
    if not password:
        password = getpass.getpass("Password: ")

    conn_info = dict(
        host=host,
        port=int(port),
        username=username,
        password=password,
    )
    qbt_client = qbittorrentapi.Client(**conn_info)

    try:
        qbt_client.auth_log_in()
    except qbittorrentapi.LoginFailed as e:
        print(e)
    else:
        print("Successfully logged in")

    print(f"qBittorrent: {qbt_client.app.version}")
    print(f"qBittorrent Web API: {qbt_client.app.web_api_version}")

    broken = []

    for torrent in qbt_client.torrents_info():
        status = [t.status for t in torrent.trackers if t.status != 0]
        status.sort()
        if len(status) == 0 or status[0] == 4:
            broken.append(torrent)

    print(f"Found {len(broken)} torrents with only broken or disabled trackers")
    if verbose > 0:
        for torrent in broken:
            print(f"{torrent.hash[-6:]}: {torrent.name} ({torrent.state})", end="")
            if delete:
                print(" [DELETING", end="")
            if with_data:
                print(" WITH DATA", end="")
            if delete:
                print("]")
            else:
                print()
            if verbose > 1:
                for tracker in torrent.trackers:
                    url = tracker.url
                    if tracker.url[0] != '*':
                        pattern = r'^(https?:\/\/[^\/]+)'
                        match = re.match(pattern, url)
                        if match:
                            url = match.group(1)
                    print(f"\t{url[:40]:<40}: {qbittorrentapi.TrackerStatus(tracker.status).display:>13}")
    torrents = [t.hash for t in broken]
    if delete:
        print(f"Deleting {len(torrents)} torrents")
        try:
            qbt_client.torrents_delete(torrent_hashes=torrents, delete_files=with_data)
        except Exception as e:
            print(f"Failed to delete torrents: {e}")

    qbt_client.auth_log_out()
    print("Logged out")

if __name__ == "__main__":
    main(sys.argv)

